<?php 

    // Mmemanggil koneksi databas 
    include '../koneksi_db.php';
    include '../function_rp.php';

    session_start();
    // ini di gunakan untuk menandakan bahwa halaman ini memiliki sesi, jika dia belum login, maka tidak boleh untuk mengakses halaman ini

    if (empty($_SESSION['nama']) AND empty($_SESSION['kode_login'])){
        header('location:../'); // jike belum login, redirect ke sini
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_SESSION['nama']; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>SMA</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>SMK_FARMASI</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['nama']; ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu Utama</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="alumni.php"><i class="fa fa-users"></i> <span>Data Alumni</span></a></li>
                <li><a href="../proses_logout.php"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
            </ul>
          <!-- /.sidebar-menu -->
        </section>
    <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">

                <div class="box-body">

                    <h1 class="text-center text-primary">Hallo, <?php echo $_SESSION['nama']; ?></h1>
                    <br />
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">

                                    <?php

                                        // Menghitung semua alumni
                                        $querySiswa    = "SELECT COUNT(idalumni) AS totalSiswa FROM alumni";
                                        $rowsSiswa     = mysqli_query($koneksi, $querySiswa);
                                        $resultSiswa   = mysqli_fetch_assoc($rowsSiswa);

                                    ?>

                                    <h3><?php echo $resultSiswa['totalSiswa']; ?></h3>
                                    <p>Total Semua Siswa</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">

                                    <?php

                                        // Menghitung semua alumni
                                        $querySiswaLakiLaki    = "SELECT COUNT(idalumni) AS totalSiswaLakiLaki FROM alumni WHERE jk='Laki-Laki'";
                                        $rowsSiswaLakiLaki     = mysqli_query($koneksi, $querySiswaLakiLaki);
                                        $resultSiswaLakiLaki   = mysqli_fetch_assoc($rowsSiswaLakiLaki);

                                    ?>

                                    <h3><?php echo $resultSiswaLakiLaki['totalSiswaLakiLaki']; ?></h3>
                                    <p>Total Siswa Laki-Laki</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-male"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">

                                    <?php

                                        // Menghitung semua alumni
                                        $querySiswiPerempuan    = "SELECT COUNT(idalumni) AS totalSiswiPerempuan FROM alumni WHERE jk='Perempuan'";
                                        $rowsSiswiPerempuan     = mysqli_query($koneksi, $querySiswiPerempuan);
                                        $resultSiswiPerempuan   = mysqli_fetch_assoc($rowsSiswiPerempuan);

                                    ?>

                                    <h3><?php echo $resultSiswiPerempuan['totalSiswiPerempuan']; ?></h3>
                                    <p>Total Siswi Perempuan</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-female"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">

                                    <?php

                                        // Menghitung semua alumni
                                        $querySukarelaAlumni    = "SELECT SUM(sukarelaalumni) AS totalSukarelaAlumni FROM alumni";
                                        $rowsSukarelaAlumni     = mysqli_query($koneksi, $querySukarelaAlumni);
                                        $resultSukarelaAlumni   = mysqli_fetch_assoc($rowsSukarelaAlumni);

                                    ?>

                                    <h3>Rp<?php echo function_rp($resultSukarelaAlumni['totalSukarelaAlumni']); ?></h3>
                                    <p>Total Sukarela Alumni</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
